#!/usr/bin/env lua

require 'Test.Assertion'

plan(8)

if not require_ok 'LIVR.Rules.Extra' then
    BAIL_OUT "no lib"
end

local m = require 'LIVR.Rules.Extra'
is_table( m, 'module' )
equals( m, package.loaded['LIVR.Rules.Extra'], 'package.loaded' )

equals( m._NAME, 'LIVR.Rules.Extra', "_NAME" )
matches( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
matches( m._DESCRIPTION, 'LIVR rules', "_DESCRIPTION" )
is_string( m._VERSION, "_VERSION" )
matches( m._VERSION, '^%d%.%d%.%d$' )

