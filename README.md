
lua-LIVR-extra
==============

Introduction
------------

This module is a [lua-LIVR](https://fperrad.frama.io/lua-LIVR) add-on,
its implements more [LIVR](https://livr-spec.org) rules.

Theses rules (with their test suite) come from [js-livr-extra-rules](https://github.com/koorchik/js-livr-extra-rules).

Links
-----

The homepage is at <https://fperrad.frama.io/lua-LIVR-extra>,
and the sources are hosted at <https://framagit.org/fperrad/lua-LIVR-extra>.

Copyright and License
---------------------

Copyright (c) 2018-2024 Francois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.

