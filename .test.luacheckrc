codes = true
read_globals = {
    -- Test.More
    'plan',
    'done_testing',
    'skip_all',
    'BAIL_OUT',
    'subtest',
    'diag',
    'note',
    'skip',
    'todo_skip',
    'skip_rest',
    'todo',
    -- Test.Assertion
    'equals',
    'is_nil',
    'is_string',
    'is_table',
    'same',
    'falsy',
    'matches',
    'require_ok',
}
